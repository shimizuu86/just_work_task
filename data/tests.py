from django.test import TestCase
from django.urls import reverse
from rest_framework.utils import json

from data.models import Page, Audio, Video


class ApiTest(TestCase):
    def setUp(self):
        self.page = Page.objects.create(title='Page 1')
        Audio.objects.create(page=self.page, title='Audio 1')
        Video.objects.create(page=self.page, title='Video 1')

    def test_get_pages_api(self):
        response = self.client.get(reverse('page-list'))
        result = json.loads(response.content)
        expected_url = reverse('page-detail', args=[self.page.id])

        self.assertEqual(result['count'], 1)
        self.assertEqual(result['results'][0]['id'], self.page.id)
        self.assertIn(expected_url, result['results'][0]['url'])

    def test_get_instance_api(self):
        response = self.client.get(reverse('page-detail', args=[self.page.id]))
        expected_url = reverse('page-detail', args=[self.page.id])
        expected_video_id = self.page.contents.all()[0].id
        result = json.loads(response.content)

        self.assertEqual(result['id'], self.page.id)
        self.assertEqual(result['contents'][0]['id'], expected_video_id)
        self.assertEqual(result['contents'][1]['bitrate'], 128)
        self.assertEqual(result['contents'][0]['comment'], '')
        self.assertEqual(len(result['contents']), 2)
        self.assertIn(expected_url, result['url'])

    def test_get_page_detail_increments_all_related_contents_counters(self):
        self.client.get(reverse('page-detail', args=[self.page.id]))
        self.assertEqual((self.page.contents.all()[0].counter,
                          self.page.contents.all()[1].counter), (1, 1))
