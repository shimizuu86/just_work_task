from django.db import models


class Page(models.Model):
    title = models.CharField(max_length=255)


class Content(models.Model):
    """Базовый класс для контента"""
    title = models.CharField(max_length=255)
    counter = models.IntegerField(default=0)
    page = models.ForeignKey(Page, related_name='contents',
                             on_delete=models.CASCADE)

    def get_child_model_data(self):
        if hasattr(self, 'video'):
            return self.video
        elif hasattr(self, 'audio'):
            return self.audio
        elif hasattr(self, 'text'):
            return self.text


class Video(Content):
    """Video Content"""
    comment = models.CharField(max_length=400)


class Audio(Content):
    """Audio Content"""
    bitrate = models.IntegerField(blank=True, default=128)


class Text(Content):
    """Text Content"""
    encoding = models.CharField(max_length=40)
