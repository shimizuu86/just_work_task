from django.contrib import admin

from data.models import Page, Video, Audio, Text, Content


class AudioInline(admin.StackedInline):
    model = Audio
    extra = 0
    fields = ['title', 'bitrate', 'counter']
    readonly_fields = ['counter', ]


class VideoInline(admin.StackedInline):
    model = Video
    extra = 0
    fields = ['title', 'comment', 'counter']
    readonly_fields = ['counter', ]


class TextInline(admin.StackedInline):
    model = Text
    extra = 0
    fields = ['title', 'encoding', 'counter']
    readonly_fields = ['counter', ]


class PageAdmin(admin.ModelAdmin):
    list_display = ['title', ]
    search_fields = ['^title', ]
    inlines = [TextInline, VideoInline, AudioInline]


class ContentAdmin(admin.ModelAdmin):
    list_display = ['title', 'page', 'counter']
    search_fields = ['^title', ]
    readonly_fields = ['counter', ]


admin.site.register(Page, PageAdmin)
admin.site.register(Content, ContentAdmin)
