from rest_framework import serializers

from .models import Page, Content, Audio, Video, Text


class AudioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Audio
        fields = ('id', 'title', 'counter', 'bitrate')


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ('id', 'title', 'counter', 'comment')


class TextSerializer(serializers.ModelSerializer):
    class Meta:
        model = Text
        fields = ('id', 'title', 'counter', 'encoding')


class ContentSerializer(serializers.ModelSerializer):
    SERIALIZERS_DICT = {
        Audio: AudioSerializer,
        Video: VideoSerializer,
        Text: TextSerializer,
    }

    class Meta:
        model = Content
        fields = ('id', 'title', 'counter')

    def to_representation(self, instance):
        instance = instance.get_child_model_data()
        serializer_class = self.SERIALIZERS_DICT.get(instance.__class__)
        super(ContentSerializer, self).to_representation(instance)
        return serializer_class(instance=instance).data


class PageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Page
        fields = ('url', 'id', 'title')


class PageInstanceSerializer(serializers.HyperlinkedModelSerializer):
    contents = ContentSerializer(many=True, read_only=True)

    class Meta:
        model = Page
        fields = ('id', 'title', 'url', 'contents')
