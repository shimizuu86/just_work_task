from rest_framework import viewsets
from rest_framework.response import Response

from data.serializers import PageSerializer, PageInstanceSerializer
from data.tasks import update_page_content_counters
from .models import Page


class PageViewSet(viewsets.ModelViewSet):
    queryset = Page.objects.all().order_by('id')
    serializer_class = PageSerializer

    def retrieve(self, request, *args, **kwargs):
        self.queryset = self.queryset.prefetch_related('contents',
                                                       'contents__audio',
                                                       'contents__video',
                                                       'contents__text')
        instance = self.get_object()
        serializer = PageInstanceSerializer(instance,
                                            context={'request': request})
        data = serializer.data
        update_page_content_counters.delay(instance.pk)
        return Response(data)
