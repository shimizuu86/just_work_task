from django.db import transaction
from django.db.models import F

from just_work_task.celery import app
from .models import Content


@app.task
@transaction.atomic
def update_page_content_counters(id):
    contents = Content.objects.filter(page_id=id)
    contents.update(counter=F('counter') + 1)
